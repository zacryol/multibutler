local-debug:
	cargo build
local-release:
	cargo build --release
linux-debug:
	cargo build --target x86_64-unknown-linux-gnu
linux-release:
	cargo build --release --target x86_64-unknown-linux-gnu
windows-debug:
	cargo build --target x86_64-pc-windows-gnu
windows-release:
	cargo build --release --target x86_64-pc-windows-gnu
debug: linux-debug windows-debug local-debug
release: linux-release windows-release local-release
all: debug release
clean:
	rm -rf target/*
install:
	cargo install --path .
uninstall:
	cargo uninstall butlerswarm
