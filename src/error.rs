/*
Copyright 2022 "zacryol"

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//! Module that defines [Error](std::error::Error) types.

use std::collections::{HashMap, HashSet};
use std::error;
use std::string::FromUtf8Error;
use std::{fmt, io};

/// Things that can go wrong when generating a [crate::PushConfig] from toml text/file.
#[derive(Debug)]
pub enum PushConfigErr {
    /// Error when attempting to read the file. Propagated from [std::fs::read_to_string].
    /// Can only be returned by [crate::PushConfig::from_toml_file].
    FileRead(io::Error),
    /// Propagated from [toml::from_str] when calling [crate::PushConfig::from_toml_text] and the TOML does not parse correctly.
    TOMLParse(toml::de::Error),
    /// "Multiple Channel Definition" - One or more channels defined multiple times (case insensitive).<br/>
    /// Contained [HashSet] lists which channels are duplicated.
    ///
    /// The fact that channels are defined as `[channels.channel-name]` means that the toml parser already handles some duplication checking,
    /// but only case sensitively. An additional check is provided for case insensitive channel name repitition, as itch.io handles channels names as case insensitive.
    ///
    /// **Note**: Channel names are currently *only* compared case insensitive in this context.
    /// At other times (namely checking if a channel is in a set), they are case sensitive.
    MultChannelDef(HashSet<String>),
    /// "Channel Set: Non-Existent Channel" - Channel set is defined to include channel that does not exist.<br/>
    /// Contained [HashMap] has the channel set name as the key, and the list of nonexistent channels as the value.
    ChSetNonExCh(HashMap<String, Vec<String>>),
}

impl From<io::Error> for PushConfigErr {
    fn from(err: io::Error) -> Self {
        Self::FileRead(err)
    }
}

impl From<toml::de::Error> for PushConfigErr {
    fn from(err: toml::de::Error) -> Self {
        Self::TOMLParse(err)
    }
}

impl fmt::Display for PushConfigErr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const DUP_CH_MSG_BASE: &str =
            "ERROR: the following channels were defined in config multiple times:\n";
        const NONEX_CH_MSG_SECOND_HALF: &str =
            // first part is str literal in format!() macro
            "includes the following nonexistent channel(s):\n";
        match self {
            Self::FileRead(err) => write!(f, "Something went wrong reading the file:\n{}", err),
            Self::TOMLParse(err) => write!(f, "Error parsing TOML: {}", err),
            Self::MultChannelDef(h) => {
                if h.is_empty() {
                    panic!("No multiple channels means no error.")
                }
                write!(
                    f,
                    "{}\n(itch.io channel names are not case sensitive)",
                    String::from(DUP_CH_MSG_BASE)
                        + h.iter()
                            .map(|s| String::from(' ') + s.as_str())
                            .collect::<String>()
                            .as_str()
                )
            }
            Self::ChSetNonExCh(h) => {
                if h.is_empty() {
                    panic!("No non-ex channels means no error.")
                }
                write!(
                    f,
                    "{}",
                    h.iter()
                        .map(|(s, v)| format!(
                            "ERROR: channel set '{}' {}",
                            s, NONEX_CH_MSG_SECOND_HALF
                        ) + v
                            .iter()
                            .map(|c| String::from(' ') + c)
                            .collect::<String>()
                            .as_str()
                            + "\n")
                        .collect::<String>()
                        .trim_end()
                )
            }
        }
    }
}

impl error::Error for PushConfigErr {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::FileRead(e) => Some(e),
            Self::TOMLParse(e) => Some(e),
            _ => None,
        }
    }
}

/// Errors that can occur when a [crate::PushConfig] tries generating a [std::process::Command] to run.
/// Currently, all possible problems are from extracting git commit hash data.
#[derive(Debug)]
pub enum CommandGenErr {
    /// `git` could not be run
    GitNotFound(io::Error),
    /// `git` exited with a non-ok status
    GitRunError,
    /// Error from converting stdout to String
    UTF8Parse(FromUtf8Error),
}

impl From<io::Error> for CommandGenErr {
    fn from(err: io::Error) -> Self {
        Self::GitNotFound(err)
    }
}

impl From<FromUtf8Error> for CommandGenErr {
    fn from(err: FromUtf8Error) -> Self {
        Self::UTF8Parse(err)
    }
}

impl From<std::process::Output> for CommandGenErr {
    fn from(o: std::process::Output) -> Self {
        assert!(
            !o.status.success(),
            "successful Output should not generate Error"
        );
        Self::GitRunError
    }
}

impl fmt::Display for CommandGenErr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::GitNotFound(_) => write!(f, "command `git` could not be run"),
            Self::GitRunError => write!(f, "`git` command exited with a non-ok status"),
            Self::UTF8Parse(_) => write!(f, "output of git was somehow not valid UTF-8"),
        }
    }
}

impl error::Error for CommandGenErr {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::GitNotFound(e) => Some(e),
            Self::UTF8Parse(e) => Some(e),
            _ => None,
        }
    }
}

/// Channel Set provided to [crate::PushConfig::execute] via the [crate::PushCommandParams] was not valid.
#[derive(Debug)]
pub struct InvalidChSetError(String);

impl From<String> for InvalidChSetError {
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl fmt::Display for InvalidChSetError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "channel set '{}' was not found in config", self.0)
    }
}

impl error::Error for InvalidChSetError {}
